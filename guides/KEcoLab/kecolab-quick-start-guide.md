# Quick Start Guide for KEcoLab

This project enables you to measure energy consumption of your software remotely using a CI/CD pipeline.

## Steps for measuring energy consumption with KEcoLab:

 1. Pre-requisites: Three usage scenario scripts are need to be created:
   - Standard Usage Scenario: It is the scenario in which the common tasks and typical user interactions are automated for an application that highlight both routine operations and resource-intensive actions.
   - Idle Scenario: It is the scenario in which the OS is running but no actions are taken.
   - Baseline Scenario: It is the scenario in which the application under consideration is opened but no actions are performed.
 2. Fork the repo present on “invent.kde.org/teams/eco/remote-eco-lab” ( an account on kde’s gitlab instance invent is required)
 3. Create a new branch in your fork and add the usage scenario scripts in the path “scripts/test_scripts/<APPLICATION_PACKAGE_NAME>/” (eg: scripts/test_scripts/org.kde.okular/log_sus.sh).
 4. Create a Merge Request using the application package name as the title. For example, org.kde.okular.

NOTE: The application name needs to be the same as that present on flathub (e.g., org.kde.kate).

## Steps for creating the usage scenario scripts

You can take a look at example scripts at <https://invent.kde.org/teams/eco/remote-eco-lab/-/tree/master/scripts/test_scripts/>. The common structure of the script is as follows:

### Common Functions

 * All scripts need to have these common functions:
   - `syncUp()`- This is used to get accurate time to be elapsed (helps in logging actions).
   - `startAction()` / `stopAction()` - This is used to output the time and action into log.csv file according to the accepted format by the software.

 * Example Code: `syncUp`

   ```
   syncUp() {
       elapsed=$((elapsed + ($1 * 1000000000)))
       delta=$(echo "scale=10; (($startTime + $elapsed) - $(date +%s%N)) / 1000000000" | bc)
       echo "Sleep" $delta
       sleep $delta
   }
   ```

 * Where,
   - elapsed = keeps track of the accumulated time in nanoseconds.
   - delta = stores the time at which the script starts executing, measured in nanoseconds.
   - bc = program performs precise arithmetic to calculate delta in seconds.

 * Example Code: `startAction` / `stopAction`

   ```
   startAction() {
      echo "iteration $1;$(date -I) $(date +%T);startAction;$2 " >> ~/log_sus.csv
   }
   ```

   ```
   stopAction() {
      echo "iteration $1;$(date -I) $(date +%T);stopAction " >> ~/log_sus.csv
   }
   ```
   
 * Where, startAction / stopAction are used as reference points for calculating elapsed time throughout the script.
   - Note: `>> log_sus.csv` (shown above) is used in the SUS script and `>> log_idle.csv` is used in the idle mode script.

## Format of Input Usage Scripts
 
   a. Standard Usage Scenario (filename: log_sus.sh): The required format for the column header is `iteration no;yy-mm-dd hh:mm:ss;startTestrun`. The sequence should be: `startTestrun`, `startAction`, `stopAction`, and `stopTestrun`.

   * Example csv output of Standard Usage Scenario:

   ```
    iteration 1;2023-08-10 18:14:44;startTestrun
    iteration 1;2023-08-10 18:14:54;startAction;go to line 100
    iteration 1;2023-08-10 18:14:59;stopAction
   ```

   b. Idle Scenario (filename: log_idle.sh): The required format is `iteration no;yy-mm-dd hh:mm:ss;startTestrun`.

   * Example csv output of Idle Scenario:
   ```
   iteration 1;2023-08-10 18:21:35;startTestrun
   iteration 1;2023-08-10 18:22:09;stopTestrun
   ```
   c. Baseline Scenario (filename: log_baseline.sh): The required format is “iteration no;yy-mm-dd hh:mm:ss;startTestrun”

   * Example csv output of baseline scenario:
   ```
   iteration 1;2023-08-10 18:20:53;startTestrun
   iteration 1;2023-08-10 18:21:13;stopTestrun
   ```
